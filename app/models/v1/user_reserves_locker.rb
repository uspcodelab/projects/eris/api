class V1::UserReservesLocker < ApplicationRecord
  belongs_to :v1_user, optional: true
  belongs_to :v1_locker, optional: true
end
