class CreateV1Positions < ActiveRecord::Migration[5.2]
  def change
    create_table :v1_positions do |t|
      t.string :position

      t.timestamps
    end
    add_index :v1_positions, :position, unique: true
  end
end
