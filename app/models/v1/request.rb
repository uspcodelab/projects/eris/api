class V1::Request < ApplicationRecord
  belongs_to :v1_contract_type, optional: true
end
