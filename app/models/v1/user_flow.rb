class V1::UserFlow < ApplicationRecord
  belongs_to :v1_user, optional: true
  belongs_to :v1_locker, optional: true
  belongs_to :v1_user_reserves_locker, optional: true
  belongs_to :v1_user_owns_locker, optional: true
  belongs_to :v1_status, optional: true
end
