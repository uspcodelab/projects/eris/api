class V1::UserFlowController < ApplicationController
  before_action :set_v1_request, only: [:show, :update, :destroy]

  # GET /v1/history
  def index
    @v1_user_flow = V1::UserFlow.all

    @history = map_status(@v1_user_flow)
    render json: @v1_user_flow
    #render json: @history
  end

  # GET /v1/user_flow/1
  def show
    render json: @v1_user_flow
  end

  # POST /v1/user_flow
  def create
    @v1_user_flow = V1::UserFlow.new(v1_user_flow_params)

    if @v1_user_flow.save
      render json: @v1_user_flow, status: :created, location: @v1_user_flow
    else
      render json: @v1_user_flow.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /v1/user_flow/1
  def update
    if @v1_user_flow.update(v1_user_flow_params)
      render json: @v1_user_flow
    else
      render json: @v1_user_flow.errors, status: :unprocessable_entity
    end
  end

  # DELETE /v1/user_flow/1
  def destroy
    @v1_user_flow.destroy
  end

	def map_status(user_flow)

		#Possible change the attributes in find
		status_phrases = {
		'registered'=> "Pedido de renovação/solicitação aceito pelo sistema\nAguardando sorteio do CAMat",
		'reserved'=> "Sorteado armário #{user_flow.find(1)["v1_locker_id"]}",
		'quitter'=> "Pagamento não recebido! Reserva cancelada.",
		'renter'=> "Pagamento recebido! Armário disponível até #{user_flow.find(1)[:v1_locker_id]}"
		}

		##When the foreign keys works well, it will be possible to render the
		##status-phase in index
 		puts status_phrases['status_id']
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_v1_user_flow
      @v1_user_flow = V1::UserFlow.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def v1_user_flow_params
      params.require(:v1_user_flow).permit(:renew, :v1_user,  
      	:v1_locker, :v1_user_reserves_locker, :v1_user_owns_locker, :v1_status)
    end
end

