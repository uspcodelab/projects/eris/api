class CreateV1UserMakesRequests < ActiveRecord::Migration[5.2]
  def change
    create_table :v1_user_makes_requests do |t|
      t.references :v1_user, foreign_key: true
      t.references :v1_request, foreign_key: true

      t.timestamps
    end
  end
end
