class V1::RequestSuggestsFeature < ApplicationRecord
  belongs_to :v1_request, optional: true
  belongs_to :v1_feature, optional: true
end
