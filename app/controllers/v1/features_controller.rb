class V1::FeaturesController < ApplicationController
  before_action :set_v1_feature, only: [:position, :place, :show, :update, :destroy]

  # GET /v1/features
  def index
    @v1_features = V1::Feature.all

    render json: @v1_features
  end

  # GET /v1/features/1/position
  def position
    render json: V1::Position.find(@v1_feature.v1_position_id)
  end

  # GET /v1/features/1/place
  def place
    render json: V1::Place.find(@v1_feature.v1_place_id)  
  end

  # GET /v1/features/1
  def show
    render json: @v1_feature
  end

  # POST /v1/features
  def create
    @v1_feature = V1::Feature.new(v1_feature_params)

    if @v1_feature.save
      render json: @v1_feature, status: :created, location: @v1_feature
    else
      render json: @v1_feature.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /v1/features/1
  def update
    if @v1_feature.update(v1_feature_params)
      render json: @v1_feature
    else
      render json: @v1_feature.errors, status: :unprocessable_entity
    end
  end

  # DELETE /v1/features/1
  def destroy
    @v1_feature.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_v1_feature
      @v1_feature = V1::Feature.find(params[:id])
      p "AQUI "
      p @v1_feature
      
    end

    # Only allow a trusted parameter "white list" through.
    def v1_feature_params
      params.require(:v1_feature).permit(:v1_position_id, :v1_place_id)
    end
end
