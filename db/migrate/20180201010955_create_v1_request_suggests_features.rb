class CreateV1RequestSuggestsFeatures < ActiveRecord::Migration[5.2]
  def change
    create_table :v1_request_suggests_features do |t|
      t.references :v1_request, foreign_key: true
      t.references :v1_feature, foreign_key: true

      t.timestamps
    end
  end
end
