# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_02_01_011001) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "v1_contract_types", force: :cascade do |t|
    t.string "contract_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["contract_type"], name: "index_v1_contract_types_on_contract_type", unique: true
  end

  create_table "v1_features", force: :cascade do |t|
    t.bigint "v1_position_id"
    t.bigint "v1_place_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["v1_place_id"], name: "index_v1_features_on_v1_place_id"
    t.index ["v1_position_id"], name: "index_v1_features_on_v1_position_id"
  end

  create_table "v1_locker_has_features", force: :cascade do |t|
    t.bigint "v1_locker_id"
    t.bigint "v1_feature_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["v1_feature_id"], name: "index_v1_locker_has_features_on_v1_feature_id"
    t.index ["v1_locker_id"], name: "index_v1_locker_has_features_on_v1_locker_id"
  end

  create_table "v1_lockers", force: :cascade do |t|
    t.string "number"
    t.boolean "available"
    t.boolean "deactivated"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["number"], name: "index_v1_lockers_on_number", unique: true
  end

  create_table "v1_places", force: :cascade do |t|
    t.string "place"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["place"], name: "index_v1_places_on_place", unique: true
  end

  create_table "v1_positions", force: :cascade do |t|
    t.string "position"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["position"], name: "index_v1_positions_on_position", unique: true
  end

  create_table "v1_request_suggests_features", force: :cascade do |t|
    t.bigint "v1_request_id"
    t.bigint "v1_feature_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["v1_feature_id"], name: "index_v1_request_suggests_features_on_v1_feature_id"
    t.index ["v1_request_id"], name: "index_v1_request_suggests_features_on_v1_request_id"
  end

  create_table "v1_requests", force: :cascade do |t|
    t.boolean "renew"
    t.bigint "v1_contract_type_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["v1_contract_type_id"], name: "index_v1_requests_on_v1_contract_type_id"
  end

  create_table "v1_statuses", force: :cascade do |t|
    t.string "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "v1_user_flows", force: :cascade do |t|
    t.bigint "v1_user_id"
    t.bigint "v1_locker_id"
    t.datetime "reserve_end_date"
    t.boolean "reserve_payment"
    t.bigint "v1_user_reserves_locker_id"
    t.datetime "ownership_end_date"
    t.bigint "v1_user_owns_locker_id"
    t.bigint "v1_status_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["v1_locker_id"], name: "index_v1_user_flows_on_v1_locker_id"
    t.index ["v1_status_id"], name: "index_v1_user_flows_on_v1_status_id"
    t.index ["v1_user_id"], name: "index_v1_user_flows_on_v1_user_id"
    t.index ["v1_user_owns_locker_id"], name: "index_v1_user_flows_on_v1_user_owns_locker_id"
    t.index ["v1_user_reserves_locker_id"], name: "index_v1_user_flows_on_v1_user_reserves_locker_id"
  end

  create_table "v1_user_makes_requests", force: :cascade do |t|
    t.bigint "v1_user_id"
    t.bigint "v1_request_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["v1_request_id"], name: "index_v1_user_makes_requests_on_v1_request_id"
    t.index ["v1_user_id"], name: "index_v1_user_makes_requests_on_v1_user_id"
  end

  create_table "v1_user_owns_lockers", force: :cascade do |t|
    t.bigint "v1_user_id"
    t.bigint "v1_locker_id"
    t.datetime "start_date"
    t.datetime "end_date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["v1_locker_id"], name: "index_v1_user_owns_lockers_on_v1_locker_id"
    t.index ["v1_user_id"], name: "index_v1_user_owns_lockers_on_v1_user_id"
  end

  create_table "v1_user_reserves_lockers", force: :cascade do |t|
    t.bigint "v1_user_id"
    t.bigint "v1_locker_id"
    t.datetime "start_date"
    t.datetime "end_date"
    t.boolean "paid"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["v1_locker_id"], name: "index_v1_user_reserves_lockers_on_v1_locker_id"
    t.index ["v1_user_id"], name: "index_v1_user_reserves_lockers_on_v1_user_id"
  end

  create_table "v1_user_roles", force: :cascade do |t|
    t.string "user_role"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "v1_users", force: :cascade do |t|
    t.string "name"
    t.string "email"
    t.integer "nusp"
    t.bigint "v1_user_roles_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["v1_user_roles_id"], name: "index_v1_users_on_v1_user_roles_id"
  end

  add_foreign_key "v1_features", "v1_places"
  add_foreign_key "v1_features", "v1_positions"
  add_foreign_key "v1_locker_has_features", "v1_features"
  add_foreign_key "v1_locker_has_features", "v1_lockers"
  add_foreign_key "v1_request_suggests_features", "v1_features"
  add_foreign_key "v1_request_suggests_features", "v1_requests"
  add_foreign_key "v1_requests", "v1_contract_types"
  add_foreign_key "v1_user_flows", "v1_lockers"
  add_foreign_key "v1_user_flows", "v1_statuses"
  add_foreign_key "v1_user_flows", "v1_user_owns_lockers"
  add_foreign_key "v1_user_flows", "v1_user_reserves_lockers"
  add_foreign_key "v1_user_flows", "v1_users"
  add_foreign_key "v1_user_makes_requests", "v1_requests"
  add_foreign_key "v1_user_makes_requests", "v1_users"
  add_foreign_key "v1_user_owns_lockers", "v1_lockers"
  add_foreign_key "v1_user_owns_lockers", "v1_users"
  add_foreign_key "v1_user_reserves_lockers", "v1_lockers"
  add_foreign_key "v1_user_reserves_lockers", "v1_users"
  add_foreign_key "v1_users", "v1_user_roles", column: "v1_user_roles_id"
end
