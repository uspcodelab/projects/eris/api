class CreateV1LockerHasFeatures < ActiveRecord::Migration[5.2]
  def change
    create_table :v1_locker_has_features do |t|
      t.references :v1_locker, foreign_key: true
      t.references :v1_feature, foreign_key: true

      t.timestamps
    end
  end
end
