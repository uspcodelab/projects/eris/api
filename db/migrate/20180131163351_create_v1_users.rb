class CreateV1Users < ActiveRecord::Migration[5.2]
  def change
    create_table :v1_users do |t|
      t.string :name
      t.string :email
      t.integer :nusp
      t.references :v1_user_roles, foreign_key: true

      t.timestamps
    end
  end
end
