# Eris System (API)

API for the Eris locker management system.

For further info, check the contribution guidelines at [CONTRIBUTING.md][1].

[1]: https://gitlab.com/eris-system/api/blob/master/CONTRIBUTING.md
